x = 4              # integer
print(x, type(x))

y = True           # boolean (True, False)
print(y, type(y))

z = 3.7            # floating point
print(z, type(z))

s = "This is a string"    # string
print(s, type(s))

x = 4            # integer
x1 = x + 4       # addition
x2 = x * 3       # multiplication
x += 2           # equivalent to x = x + 2
x3 = x
x *= 3           # equivalent to x = x * 3
x4 = x
x5 = x % 4       # modulo (remainder) operator

z = 3.7          # floating point number
z1= z - 2       # subtraction
z2 = z / 3       # division
z3 = z // 3      # integer division
z4 = z ** 2      # square of z
z5 = z4 ** 0.5   # square root
z6 = pow(z,2)    # equivalent to square of z
z7 = round(z)    # rounding z to its nearest integer
z8 = int(z)      # type casting float to int

list = [1,2,3,4,5]

list.pop()

print(list)

print(x,x1,x2,x3,x4,x5)
print(z,z1,z2,z3,z4)
print(z5,z6,z7,z8)
print(4.2-3)