from sklearn import metrics
from math import sqrt

### CIs (confidence intervals)
def roc_auc_ci(y_true, y_score, positive=1):
    AUC = metrics.roc_auc_score(y_true, y_score)
    N1 = sum(y_true == positive)
    N2 = sum(y_true != positive)
    Q1 = AUC / (2 - AUC)
    Q2 = 2 * AUC**2 / (1 + AUC)
    SE_AUC = sqrt(
        (AUC * (1 - AUC) + (N1 - 1) * (Q1 - AUC**2) + (N2 - 1) * (Q2 - AUC**2))
        / (N1 * N2)
    )
    print(SE_AUC)

    ## Zcrit = 1.96 when a = 0.05 (95% confidence intervals)
    lower = AUC - 1.96 * SE_AUC
    upper = AUC + 1.96 * SE_AUC
    if lower < 0:
        lower = 0
    if upper > 1:
        upper = 1
    return lower, upper


# roc_aus_ci()
