import pandas as pd

# read snp sift prediction from pca and bayescan overlapping
pca_bayescan_sift_df = pd.read_csv(
    "/Users/ericliao/Desktop/WNV_project_files/landscape_genetics/Paper_results/"
    "sift_prediciton_results/match_snps_sift_results/"
    "pca_bayescan_overlapping_snps_sift_results.csv",
    header=0,
    index_col=False,
    sep=",",
)
# drop the candidate_source column
pca_bayescan_sift_df = pca_bayescan_sift_df.drop(columns=["candidate_source"])

# read snp sift prediction from lfmm pc1 and rda overlapping
lfmm_rda_sift_df = pd.read_csv(
    "/Users/ericliao/Desktop/WNV_project_files/landscape_genetics/Paper_results/"
    "sift_prediciton_results/match_snps_sift_results/"
    "lfmm_pc1_rda_overlapping_snps_sift_results.csv",
    header=0,
    index_col=False,
    sep=",",
)

# drop the candidate_source column
lfmm_rda_sift_df = lfmm_rda_sift_df.drop(columns=["candidate_source"])

## get common rows between pca_bayescan_sift_df and lfmm_rda_sift_df
common_sift_df = pd.merge(
    pca_bayescan_sift_df,
    lfmm_rda_sift_df,
    how="inner",
    on=[
        "CHROM",
        "POSITION",
        "REF_ALLELE",
        "ALT_ALLELE",
        "TRANSCRIPT_ID",
        "GENE_ID",
        "GENE_NAME",
        "REGION",
        "VARIANT_TYPE",
        "REF_AA",
        "ALT_AA",
        "AA_POS",
        "SIFT_SCORE",
        "SIFT_MEDIAN",
        "NUM_SEQs",
        "dbSNP",
        "PREDICTION",
    ],
)

common_sift_df.to_csv(
    "/Users/ericliao/Desktop/WNV_project_files/landscape_genetics/Paper_results/"
    "sift_prediciton_results/match_snps_sift_results/"
    "overlapping_landscape_outlier_analysis_snp_sift_prediction.csv",
    index=False,
)


# drop PREDICTION column where value is nan
common_sift_df = common_sift_df.dropna(subset=["PREDICTION"])
# get PREDICTION column where value contains DELETERIOUS
common_sift_df_deleterious = common_sift_df[
    common_sift_df["PREDICTION"].str.contains("DELETERIOUS")
]

common_sift_df_deleterious.to_csv(
    "/Users/ericliao/Desktop/WNV_project_files/landscape_genetics/Paper_results/"
    "sift_prediciton_results/match_snps_sift_results/"
    "overlapping_landscape_outlier_analysis_snp_sift_prediction_deleterious.csv",
    index=False,
)

# ## get gene list and build gene_list value by append 20471- in front of each gene name and .m01 at the end
gene_list = []
for gene_entry in common_sift_df_deleterious["TRANSCRIPT_ID"]:
    gene_list.append("20471-" + gene_entry)

## look up the gene name from the gene list
df_interproscan = pd.read_csv(
    "/Users/ericliao/Desktop/WNV_project_files/landscape_genetics/original_vcf/"
    "Culex-tarsalis-v1.0.a1.5d6405151b078-interproscan.tab",
    header=8,
    index_col=False,
    sep="\t",
)

# from df_interproscan, the Name column contains the characters that we want to match in the gene_list. get the rows that contains gene_list values as part of the valu
df_interproscan_gene = df_interproscan[df_interproscan["Name"].isin(gene_list)]

df_interproscan_gene.to_csv(
    "/Users/ericliao/Desktop/WNV_project_files/landscape_genetics/Paper_results/"
    "compare_landscape_and_outlier_analysis/"
    "landscape_outlier_analysis_common_gene_sift_deleterious.csv",
    index=False,
)
